package net.pl3x.bukkit.picturemap.frame;

import net.pl3x.bukkit.picturemap.Main;
import net.pl3x.bukkit.picturemap.renderer.ImageRenderer;
import net.pl3x.bukkit.picturemap.renderer.TextRenderer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.ItemFrame;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Picture {
	private Short mapId;
	private String path;

	Picture(String path, Short mapId) {
		this.mapId = mapId;
		this.path = path;
	}

	Short getMapId() {
		return mapId;
	}

	public String getPath() {
		return path;
	}

	private Image getPicture(Main plugin) {
		File imageFolder = new File(plugin.getDataFolder() + File.separator + "images");
		Image image;
		if ((path.startsWith("http://")) || (path.startsWith("https://")) || (path.startsWith("ftp://"))) {
			URL url;
			try {
				url = new URL(path);
				image = ImageIO.read(url);
			} catch (Exception e) {
				return null;
			}
			if (image == null)
				return null;
			return image.getScaledInstance(128, 128, 1);
		}
		File file = new File(imageFolder, path);
		if (!file.exists())
			return null;
		try {
			image = ImageIO.read(file);
		} catch (Exception e) {
			return null;
		}
		if (image == null)
			return null;
		return image.getScaledInstance(128, 128, 1);
	}

	@SuppressWarnings("deprecation")
	void update(Main plugin) {
		MapView view = Bukkit.getMap(mapId);
		if (view == null) {
			short newId = Bukkit.createMap(Bukkit.getWorlds().get(0)).getId();
			List<ItemFrame> list = new ArrayList<>();
			/*
			for (ItemFrame frame : Bukkit.getWorlds().get(0).getEntitiesByClass(ItemFrame.class)) {
				if ((frame.getItem() != null) && (frame.getItem().getType() == Material.MAP))
					if (frame.getItem().getDurability() == mapId)
						list.add(frame);
			}
			 */
			list.addAll(Bukkit.getWorlds().get(0).getEntitiesByClass(ItemFrame.class).stream().filter(frame -> (frame.getItem() != null) && (frame.getItem().getType() == Material.MAP)).filter(frame -> frame.getItem().getDurability() == mapId).collect(Collectors.toList()));
			for (ItemFrame frame : list) {
				ItemStack item = frame.getItem();
				item.setDurability(newId);
				frame.setItem(item);
			}
			plugin.debug("Picture #" + mapId + " has a new Id: #" + newId);
			mapId = newId;
			view = Bukkit.getMap(newId);
		}
		for (MapRenderer render : view.getRenderers()) {
			view.removeRenderer(render);
		}
		MapView view2 = view;
		Image image = getPicture(plugin);
		if (image == null) {
			plugin.log("The Url \"" + getPath() + "\" from Frame #" + getMapId().toString() + " does not exists!");
			view2.addRenderer(new TextRenderer("404 Image Not Found!", getMapId()));
			return;
		}
		MapRenderer renderer = new ImageRenderer(plugin, image);
		view2.addRenderer(renderer);
		plugin.getFrameManager().sendMap(this);
	}
}
