package net.pl3x.bukkit.picturemap.frame;

import net.pl3x.bukkit.picturemap.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MapView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FrameManager {
	private Main plugin;
    private HashMap<Short, Picture> pictures = new HashMap<>();

	public FrameManager(Main plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	public Picture addPicture(String path, ItemFrame iFrame) {
        Picture picture = new Picture(path, Bukkit.createMap(Bukkit.getWorlds().get(0)).getId());
        pictures.put(picture.getMapId(), picture);
        picture.update(plugin);
		iFrame.setItem(new ItemStack(Material.MAP, 1, picture.getMapId()));
		savePictures();
		return picture;
	}

	public Picture getPicture(short mapId) {
		return pictures.get(mapId);
	}

    private List<Picture> getPictures() {
        List<Picture> list = new ArrayList<>();
        list.addAll(pictures.values());
        return list;
	}

	public boolean isPictureFrame(Entity entity) {
		if (entity.getType() != EntityType.ITEM_FRAME)
			return false;
		ItemFrame iFrame = (ItemFrame) entity;
        /* ** Attempt to fix annoying warning **
        if (iFrame.getItem().getType() != Material.MAP)
			return false;
		return pictures.containsKey(iFrame.getItem().getDurability());*/
        return iFrame.getItem().getType() == Material.MAP && pictures.containsKey(iFrame.getItem().getDurability());
    }

	@SuppressWarnings("deprecation")
	public boolean removePicture(short mapId) {
		Picture picture = getPicture(mapId);
		if (picture == null)
			return false;
		MapView view = Bukkit.getMap(picture.getMapId());
        view.getRenderers().forEach(view::removeRenderer);
        pictures.remove(mapId);
        try {
            File worldFolder = new File(Bukkit.getWorlds().get(0).getName());
            File dataFolder = new File(worldFolder, "data");
            File mapFile = new File(dataFolder, "map_" + mapId + ".dat");
			if (mapFile.exists())
				mapFile.delete();
		} catch (Exception e) {
			plugin.log("&4Can't remove the Map Data from #" + mapId);
		}
		savePictures();
		return true;
	}

	public void sendMaps(Player player) {
		for (Picture picture : getPictures())
			sendMap(picture, player);
	}

    void sendMap(Picture picture) {
        for (Player player : Bukkit.getOnlinePlayers())
            sendMap(picture, player);
	}

	@SuppressWarnings("deprecation")
    private void sendMap(Picture picture, Player player) {
        MapView map = Bukkit.getMap(picture.getMapId());
        if (map == null || map.getRenderers().isEmpty())
			return;
		player.sendMap(map);
	}

	public void loadPictures() {
		YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder() + File.separator + "pictures.yml"));
		pictures.clear();
		for (String key : config.getKeys(false)) {
			short mapId = Short.parseShort(key);
			String path = config.getString(key);
			Picture picture = new Picture(path, mapId);
			pictures.put(mapId, picture);
			picture.update(plugin);
		}
		plugin.debug("Loaded " + pictures.size() + " pictures!");
	}

	public void savePictures() {
		YamlConfiguration config = new YamlConfiguration();
		for (Picture picture : getPictures()) {
			config.set(picture.getMapId().toString(), picture.getPath());
		}
		try {
			config.save(new File(plugin.getDataFolder() + File.separator + "pictures.yml"));
		} catch (Exception e) {
			plugin.log("&4Error while saving the pictures!");
			e.printStackTrace();
		}
	}
}
