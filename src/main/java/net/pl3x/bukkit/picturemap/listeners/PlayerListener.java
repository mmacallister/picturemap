package net.pl3x.bukkit.picturemap.listeners;

import net.pl3x.bukkit.picturemap.Main;
import net.pl3x.bukkit.picturemap.frame.Picture;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {
	private Main plugin;

	public PlayerListener(Main plugin) {
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onRightClickFrame(PlayerInteractEntityEvent event) {
		Entity entity = event.getRightClicked();
		if (!entity.getType().equals(EntityType.ITEM_FRAME))
			return;
		ItemFrame itemFrame = (ItemFrame) entity;
		Picture picture = itemFrame.getItem() != null ? plugin.getFrameManager().getPicture(itemFrame.getItem().getDurability()) : null;
		// null check here?
		Player player = event.getPlayer();
		if (plugin.getPlayerAction().hasCreate(event.getPlayer())) {
			event.setCancelled(true);
			if (!player.hasPermission("picturemap.create")) {
				player.sendMessage(plugin.colorize("&4You do not have permission for that!"));
				return;
			}
			/*if (picture != null && !itemFrame.getItem().equals(Material.AIR)) {
				player.sendMessage(plugin.colorize("&4That is not an empty item frame!"));
				return;
			}*/
			String path = plugin.getPlayerAction().getCreate(player);
			picture = plugin.getFrameManager().addPicture(path, itemFrame);
			if (picture == null)
				return;
			plugin.getPlayerAction().removeCreate(player);
			player.sendMessage(plugin.colorize("&6Picture has been created."));
		}
		if (plugin.getPlayerAction().hasInfo(event.getPlayer())) {
			if (!player.hasPermission("picturemap.info")) {
				player.sendMessage(plugin.colorize("&4You do not have permission for that!"));
				return;
			}
			if (picture == null) {
				player.sendMessage(plugin.colorize("&4That is not a picture!"));
				return;
			}
			player.sendMessage(plugin.colorize("&6Info:&r " + picture.getPath()));
			event.setCancelled(true);
		}

	}

	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onHangingBreak(HangingBreakByEntityEvent event) {
		Entity entity = event.getEntity();
		if (!plugin.getFrameManager().isPictureFrame(entity))
			return;
		ItemFrame iFrame = (ItemFrame) entity;
		plugin.getFrameManager().removePicture(iFrame.getItem().getDurability());
		if (event.getRemover().getType() == EntityType.PLAYER) {
			Player player = (Player) event.getRemover();
			player.sendMessage(plugin.colorize("&6Picture removed."));
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if (plugin.getPlayerAction().hasCreate(player))
			plugin.getPlayerAction().removeCreate(player);
		if (plugin.getPlayerAction().hasInfo(player))
			plugin.getPlayerAction().removeInfo(player);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerJoin(PlayerJoinEvent event) {
		plugin.getFrameManager().sendMaps(event.getPlayer());
	}
}
