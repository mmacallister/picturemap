package net.pl3x.bukkit.picturemap.renderer;

import java.awt.Image;

import net.pl3x.bukkit.picturemap.Main;

import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

public class ImageRenderer extends MapRenderer {
	private Main plugin;
	private Image image;
	private boolean rendered;

	public ImageRenderer(Main plugin, Image image) {
		this.plugin = plugin;
		this.image = image;
	}

	public Image getImage() {
		return image;
	}

	public boolean isRendered() {
		return rendered;
	}

	private void removeCursors(MapCanvas canvas) {
		for (int i = 0; i < canvas.getCursors().size(); i++) {
			canvas.getCursors().removeCursor(canvas.getCursors().getCursor(i));
		}
	}

	@Override
	public void render(MapView map, MapCanvas canvas, Player player) {
		if (isRendered())
			return;
		removeCursors(canvas);
		rendered = true;
		try {
			canvas.drawImage(0, 0, image);
			player.sendMap(map);
		} catch (Exception e) {
			plugin.debug("Error drawing map: " + e.getMessage());
		}
	}

}
