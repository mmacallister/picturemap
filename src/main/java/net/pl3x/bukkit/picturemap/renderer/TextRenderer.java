package net.pl3x.bukkit.picturemap.renderer;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

public class TextRenderer extends MapRenderer {
	private String text;
	private Short mapId = null;
	private boolean rendered = false;

	public TextRenderer(String text) {
		this.text = text;
	}

	public TextRenderer(String text, Short mapId) {
		this.text = text;
		this.mapId = mapId;
	}

	public String getText() {
		return text;
	}

	public Short getMapId() {
		return mapId;
	}

	public boolean isRendered() {
		return rendered;
	}

	private void removeCursors(MapCanvas canvas) {
		for (int i = 0; i < canvas.getCursors().size(); i++) {
			canvas.getCursors().removeCursor(canvas.getCursors().getCursor(i));
		}
	}

	public void render(MapView map, MapCanvas canvas, Player player) {
		if (rendered)
			return;
		removeCursors(canvas);
		rendered = true;
		try {
			BufferedImage image = new BufferedImage(128, 128, 2);
			Graphics gfx = image.getGraphics();
			gfx.drawString(text, 5, 12);
			if (mapId != null)
				gfx.drawString("Map #" + mapId.toString(), 70, 115);
			canvas.drawImage(0, 0, image);
			player.sendMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
