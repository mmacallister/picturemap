package net.pl3x.bukkit.picturemap.utils;

import net.pl3x.bukkit.picturemap.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlayerAction {
	private Main plugin;
	private HashMap<Player, String> create = new HashMap<>();
	private List<Player> info = new ArrayList<>();

	public PlayerAction(Main plugin) {
		this.plugin = plugin;
	}

	public boolean hasCreate(Player player) {
		return create.containsKey(player);
	}

	public String getCreate(Player player) {
		return create.get(player);
	}

	public void addCreate(Player player, String path) {
		create.put(player, path);
		Bukkit.getScheduler().runTaskLater(plugin, new TimeOut(player, path), 600); // 30 seconds
	}

	public void removeCreate(Player player) {
		create.remove(player);
	}

	public boolean hasInfo(Player player) {
		return info.contains(player);
	}

	public void addInfo(Player player) {
		info.add(player);
		Bukkit.getScheduler().runTaskLater(plugin, new TimeOut(player, null), 600); // 30 seconds
	}

	public void removeInfo(Player player) {
		info.remove(player);
	}

	private class TimeOut implements Runnable {
		private Player player;
		private String path;

		TimeOut(Player player, String path) {
			this.player = player;
			this.path = path;
		}

		@Override
		public void run() {
			if (path == null) {
				if (create.containsKey(player))
					create.remove(player);
				return;
			}
			if (info.contains(player))
				info.remove(player);
		}
	}
}
