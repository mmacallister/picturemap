package net.pl3x.bukkit.picturemap;

import net.pl3x.bukkit.picturemap.commands.CmdPictureMap;
import net.pl3x.bukkit.picturemap.frame.FrameManager;
import net.pl3x.bukkit.picturemap.listeners.PlayerListener;
import net.pl3x.bukkit.picturemap.utils.PlayerAction;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.logging.Level;

public class Main extends JavaPlugin {
	private FrameManager frameManager;
	private PlayerAction playerAction;

	public void onEnable() {
		if (!new File(getDataFolder() + File.separator + "config.yml").exists())
			saveDefaultConfig();

		frameManager = new FrameManager(this);
		playerAction = new PlayerAction(this);

		frameManager.loadPictures();

		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);

		getCommand("picturemap").setExecutor(new CmdPictureMap(this));

		log(getName() + " v" + getDescription().getVersion() + " by BillyGalbreath enabled!");
	}

	public void onDisable() {
		if (frameManager != null)
			frameManager.savePictures();
		log(getName() + " Disabled.");
	}

	public void log(Object obj) {
		if (getConfig().getBoolean("color-logs", true)) {
			getServer().getConsoleSender().sendMessage(colorize("&4[&7" + getName() + "&4]&r " + obj));
		} else {
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
		}
	}

	public void debug(Object obj) {
		if (getConfig().getBoolean("debug-mode", false))
			log(obj);
	}

	public String colorize(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}

	public FrameManager getFrameManager() {
		return frameManager;
	}

	public PlayerAction getPlayerAction() {
		return playerAction;
	}
}
