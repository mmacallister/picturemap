package net.pl3x.bukkit.picturemap.commands;

import net.pl3x.bukkit.picturemap.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdPictureMap implements CommandExecutor {
	private Main plugin;

	public CmdPictureMap(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (!cmd.getName().equalsIgnoreCase("PictureMap"))
			return false;
		if (!(cs instanceof Player)) {
			cs.sendMessage(plugin.colorize("&4This command is only available to players!"));
			return true;
		}
		Player player = (Player) cs;
		if (args.length == 0) {
			cs.sendMessage(plugin.colorize("&4Error: create or info?"));
			return false;
		}
		if (args[0].equalsIgnoreCase("info")) {
			if (!cs.hasPermission("picturemap.info")) {
				cs.sendMessage(plugin.colorize("&4You do not have permission for that command!"));
				plugin.log(cs.getName() + " was denied access to that command!");
				return true;
			}
			if (plugin.getPlayerAction().hasInfo(player)) {
				plugin.getPlayerAction().removeInfo(player);
				player.sendMessage(plugin.colorize("&6Cancelled info check."));
			} else {
				plugin.getPlayerAction().addInfo(player);
				player.sendMessage(plugin.colorize("&6Right click on picture to check its info."));
			}
			return true;
		}
		if (args[0].equalsIgnoreCase("create")) {
			if (!cs.hasPermission("picturemap.info")) {
				cs.sendMessage(plugin.colorize("&4You do not have permission for that command!"));
				plugin.log(cs.getName() + " was denied access to that command!");
				return true;
			}
			if (args.length == 1 && plugin.getPlayerAction().hasCreate(player)) {
				plugin.getPlayerAction().removeCreate(player);
				player.sendMessage(plugin.colorize("&6Cancelled picture creation."));
				return true;
			}
			if (args.length < 2) {
				cs.sendMessage(plugin.colorize("&4Please specify an image to create!"));
				return false;
			}
			if (plugin.getPlayerAction().hasCreate(player)) {
				cs.sendMessage(plugin.colorize("&4You are already in the creation process!"));
				return true;
			}
			plugin.getPlayerAction().addCreate(player, args[1].trim());
			cs.sendMessage(plugin.colorize("&6Right click an item frame to create the picture."));
			return true;
		}
		cs.sendMessage(plugin.colorize("&4Unrecognized sub-command!"));
		return false;
	}
}
